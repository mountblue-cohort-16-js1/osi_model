# OSI Model

In 1984, The International Standards Organization (ISO) proposed a model name Open Systems Interconnection (OSI) with the original objective to provide a standard design to manufacturers to equip their operating system with platform-independent networking features.

In simple terms, OSI Model was introduced to resolve the communication issues between diversified computer systems, architectures, or interfaces.

It is comprised of seven hierarchical layers which offer wide benefits. Each layer is a package of the protocol. The divide-and-Conquer methodology was used here to solve the network problems by splitting these into smaller logical segments.

|  Layers | Acronyms  |
| :------------: | :------------: |
| Application | **A**ll  |
| Presentation  | **P**eople  |
| Session  | **S**eem  |
| Transport | **T**o  |
| Network  | **N**eed  |
| Data-Link | **D**ata  |
| Physical | **P**rocessing  |

![osi_model](images/OSI_models.jpg "OSI Model")

## The Upper Layers

The top three layers of the OSI model are often referred to as the upper layers.
At these layers, protocols manage application-level functions and are implemented in *software*.

### Application Layer (Layer-7)

Application Layer is used by network applications like web browsers. These applications don't reside in this layer but protocols like *HTTP*, *HTTPS* are used in web surfing.
Dozens of protocol collectives form this entire Layer. For example:

- *FTP*: File transfer
- *HTTPS*: Web surfing
- *SMTP*: E-mailing
- *Telnet*: Virtual terminal

![layer](images/lays7.jpg "layer")

### Presentation Layer (Layer-6)

Presentation Layer receives data in form of characters and numbers from Application Layer and converts them into machine code as a part of the **Translation** process.
Then **Data Compression** occurs as a bit reduction process and results are either *lossy* or *lossless*. The reduced size helps in faster real-time streaming.
This layer also maintains the integrity of the data by encrypting it at the sender's site and then decrypting it at the receiver's using the technique called **SSL** (Secure Sockets Layer).

![layer](images/lays6.jpg "layer")

### Session Layer (Layer-5)

Session Layer helps in setting up, managing, and terminating the connection as required.
Application Interface (API) like NetBIOS could be a good example.

*Authentication* is performed by this layer after making the user input their username and respective password.
Followed by a proper *Authorization* which involves checking the permissions to access the file which is being requested.
This layer also keeps a track of downloads from the website's servers.

![layer](images/lays5.jpg "layer")

## The Lower Layers

The bottom four layers of the OSI model are often referred to as the lower layers. Protocols that operate at these layers control the end-to-end transporting of data. And are implemented in both *software* and *hardware*.

### Transport Layer (Layer-4)

Transport Layer controls reliability by providing logical communication between application processes running on different hosts.
The operations performed are:

- Segmentation
- Flow Control
- Error Control

Segmentation is the dividing of data into segments that contains **Sequence number** to maintain the queue order and **Port numbers** to track sender and receiver.
Through Flow Control, the amount of data being transmitted is kept in the account.
And Error Control uses automatic repeat request schemes to re-transmit any misplaced data.

Checksum (a group of bits) is added to find out the received data segment is corrupted.
This layer uses two protocols named _TCP_ and _UDP_.
Its services are named as Connection-Oriented (via TCP) and Connection-Less (via UDP).
UDP doesn't provide feedback so it's faster but TCP can track any lost data using its feedback feature.

![layer](images/lays4.jpg "layer")

### Network Layer (Layer-3)

Network Layer works for transmission of the segment from one computer to another.
Its primary functions are:

- Logical Addressing
- Routing
- Path Determination

The IP addresses of sender and receiver are assigned to data unit packets (Segments) via Logical Addressing.
The routing method is then used to deliver the data packet.
Finally, Path determination is applied to find the best possible path delivery. Protocols used here are **_OSPF, IS-IS, BGP._**

![layer](images/lays3.jpg "layer")

### Data-Link Layer (Layer-2)

In Data-Link Layer, there are two addressing methods are used in OSI Model; logical and physical.

Physical Addressing is done in this layer where MAC addresses are assigned to data packets to form frames.
MAC address is 12 digits alpha-numeric value created by manufacturers which are embedded in NIC (Network Interface Controller).
Any transfer is done via local media only (LAN, Optical Fibre, Copper Wires).

It performs two functions:

- **Framing**: Allows upper layer of OSI to access media
- **Media Access Control & Error Detection** Controls how data is placed and received from the media then bits in the tails are used to detect errors

CSMA (Carrier-sense multiple access) protocols are used to keep an eye on preventing the mix-up of data through local media.

![layer](images/lays2.jpg "layer")

### Physical Layer (Layer-1)

In Physical Layer, the data received is *segmented, packet-ed* and *framed* which are ultimately the bits of a binary sequence.
This layer converts the bits into signals!
They can be electric, light, or radio signal and its all depends on the type of media it is connected with.

![layer](images/lays1.jpg "layer")

-------------------------------------

#### *References:*

[OSI Model Explained (YouTube)](https://www.youtube.com/watch?v=vv4y_uOneC0 "OSI Model Explained (YouTube)")

Report format motivations: [link1](https://www.sans.org/reading-room/whitepapers/standards/osi-model-overview-543 "link1") & [link2](https://www.routeralley.com/guides/osi.pdf "link2")

[Illustrations](https://www.lifewire.com/layers-of-the-osi-model-illustrated-818017 "Illustrations")
